import React from 'react';
import { ILed } from '../../shared/types';
import './led-list.css';
import Led from '../led/led';

interface LedListProps {
 ledList: ILed[];
}

const LedList = ({ ledList }: LedListProps) => {
 return (
  <div className={'ledList'}>
   {ledList &&
    ledList.map((led) => {
     return <Led key={led.id} led={led} />;
    })}
  </div>
 );
};

export default LedList;
