import React, { useContext } from 'react';
import './led.css';
import { ITreeContext, TreeContext } from '../../shared/TreeContextProvider';
import { ILed } from '../../shared/types';
import LedList from '../led-list/led-list';

interface LedProps {
 led: ILed;
}

const Led = ({ led }: LedProps) => {
 const { update } = useContext<ITreeContext>(TreeContext);
 return (
  <div>
   <React.Fragment>
    <button
     onClick={() => update(led.id, !led.buttonState)}
     className={led.buttonState ? 'led backgroundOn' : 'led backgroundOff'}
    >
     {led.label}
    </button>
   </React.Fragment>
   {led.ledList && led.ledList.length > 0 && <LedList ledList={led.ledList} />}
  </div>
 );
};

export default Led;
