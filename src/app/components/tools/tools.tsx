import React, { useContext, useState } from 'react';
import './tools.css';
import { TreeContext } from '../../shared/TreeContextProvider';

interface ToolsProps {
 onCreate: Function;
}

export const Tools = ({ onCreate }: ToolsProps) => {
 const [buttonLabel, setButtonLabel] = useState('Create Tree');
 const { destroy } = useContext(TreeContext);

 const onClick = () => {
  if (buttonLabel === 'Create Tree') {
   setButtonLabel('Destroy Tree');
   onCreate('create');
  } else {
   setButtonLabel('Create Tree');
   onCreate('destroy');
   destroy();
  }
 };

 return (
  <div className={'toolsRow'}>
   <div className={'toolItem'}>
    <h1 className="weihnachsDOMLabel">WeihnachtsDOM</h1>
   </div>
   <div className={'toolItem'}>
    <button onClick={() => onClick()} className={'toolsButton'}>
     {buttonLabel}
    </button>
   </div>
  </div>
 );
};

export default Tools;
