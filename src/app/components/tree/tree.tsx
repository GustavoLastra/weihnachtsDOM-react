import React, { useContext, useState } from 'react';

import './tree.css';
import { TreeContext, ITreeContext } from '../../shared/TreeContextProvider';
import Tools from '../tools/tools';
import LedList from '../led-list/led-list';

export const Tree = () => {
 const { tree } = useContext<ITreeContext>(TreeContext);
 const [showTree, setShowTree] = useState<boolean>(false);

 const onCreate = (todo: 'create' | 'destroy') => {
  todo === 'create' ? setShowTree(true) : setShowTree(false);
 };

 return (
  <div className={'rootContainer'}>
   <div className={'toolsContainer'}>
    <Tools onCreate={onCreate} />
   </div>
   {showTree && (
    <div className={'treeContainer'}>
     <React.Fragment>
      <LedList ledList={tree} />
     </React.Fragment>
    </div>
   )}
   {!showTree && (
    <div className="hintContainer">
     <div className="hint">Please click on the "Create Tree" button</div>
    </div>
   )}
  </div>
 );
};
