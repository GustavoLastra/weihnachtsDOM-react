import React from 'react';
import { Tree } from './components/tree/tree';
import TreeContextProvider from './shared/TreeContextProvider';

export const App = () => {
 return (
  <TreeContextProvider>
   <Tree />
  </TreeContextProvider>
 );
};
