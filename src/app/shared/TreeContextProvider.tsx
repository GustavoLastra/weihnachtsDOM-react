import React, { ReactNode, useState, createContext } from 'react';
import { InitialledList } from './data';
import { ILed } from './types';
import _ from 'lodash';

type ProviderProps = {
 children: ReactNode;
};

export interface ITreeContext {
 tree: ILed[];
 update: Function;
 destroy: Function;
}
const defaultState: ITreeContext = {
 tree: [],
 update: () => {},
 destroy: () => {},
};

export const TreeContext = createContext<ITreeContext>(defaultState);

const TreeContextProvider = ({ children }: ProviderProps) => {
 const [tree, setTree] = useState<ILed[]>(InitialledList);

 const resetTree = () => {
  turn('1', false);
 };

 const turn = (id: string, newButtonState: boolean) => {
  let found = false;
  let treeCopy: ILed[] = _.clone(tree);
  scan(treeCopy, id, newButtonState, found);
 };

 const scan = (ledList: ILed[], id: string, newButtonState: boolean, found: boolean): any => {
  if (found) {
   ledList.forEach((led: ILed) => {
    led.buttonState = newButtonState;
    updateLedState(led);
    scan(led.ledList, id, newButtonState, found);
   });
  } else {
   ledList.forEach((led: ILed) => {
    if (led.id === id) {
     found = true;
     led.buttonState = newButtonState;
     updateLedState(led);
     return scan(led.ledList, id, newButtonState, found);
    } else if (!found) {
     return scan(led.ledList, id, newButtonState, found);
    }
   });
  }
 };

 const updateLedState = (ledWithNewStatus: ILed) => {
  const newTree = _.cloneDeepWith(tree, (led: ILed) => {
   return led.id === ledWithNewStatus.id ? { ...led, ...ledWithNewStatus } : _.noop();
  });
  setTree(newTree);
 };

 return (
  <TreeContext.Provider
   value={{
    tree,
    update: (id: string, newButtonState: boolean) => turn(id, newButtonState),
    destroy: () => resetTree(),
   }}
  >
   {children}
  </TreeContext.Provider>
 );
};

export default TreeContextProvider;
