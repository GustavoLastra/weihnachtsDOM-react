export interface ILed {
 id: string;
 label: string;
 buttonState: boolean;
 ledList: ILed[];
}
